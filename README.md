# Partner Technical Bootcamp: Using GitLab

&nbsp;
* The [complete bootcamp](https://about.gitlab.com/handbook/resellers/bootcamp/index.html) is composed of two different sections: "Using GitLab" and "Implementing GitLab".
* This project includes all labs to be carried out in the "Using GitLab" Bootcamp
* You will require access to a GitLab instance to run this bootcamp. If you, or your organization, do not have a private **Ultimate** instance available, you can use [GitLab.com](https://www.gitlab.com)
* Labs take advantage of paid tier features; to get the full experience your instance of GitLab, or your **Group** if using gitlab.com, should be licensed to Ultimate/Gold. 
  * **Get a trial license for gitlab.com** [here](https://about.gitlab.com/free-trial/) - make sure that on the 2nd page you use the dropdown to specify one of your existing Groups, or you create a new group, for this activity, rather than just using your personal gitlab namespace.  Below you will create a further sub-group in which to carry out the Bootcamp labs.  
&nbsp;
* Lab exercises are defined as individual **GitLab Issues** within this project  
* Once you have an instance of GitLab available, or you have a Gold-tier group on gitlab.com, create a new [Group](https://docs.gitlab.com/ee/user/group/) and name it **Bootcamp** 
* [Import](https://docs.gitlab.com/ee/user/project/settings/import_export.html#importing-the-project) the latest export of the [project containing the Lab exercises](https://drive.google.com/file/d/195xB0wbQzYOkOZ3EVJnyJGCBNLo6azzi/view?usp=sharing) into your **Bootcamp** Group, or alternatively you can manually copy and paste the issues into a new project in that Group. 

* Many of the Lab exercises utilise a simple **Coffee Shop** application;  You need a copy of [the Coffee Shop project](https://gitlab.com/technical-bootcamp/gitlab-coffee-shop); if using GitLab.com you can [fork the project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) (be sure to remove the fork relationship once your project is created), or if you are using your own GitLab instance you can [clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) the project, or [download the code](https://docs.gitlab.com/ee/user/project/repository/#download-source-code), to then populate your own Coffee Shop project in your own instance.  The project should be created within your **Bootcamp** group.  
* Once you have both the Labs and Coffee Shop projects creating within your **Bootcamp** group, you can start working through the Lab exercises, starting with Lab 0, as described on the main Bootcamp [page](https://about.gitlab.com/handbook/resellers/bootcamp/index.html).
